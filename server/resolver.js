var installedObj = require('../installed.json');
var uninstalledObj = require('../uninstalled.json');
const root = {
    installed: () => {
        return installedObj.data.installedSdks;
    },
    uninstalled: () => {
        return uninstalledObj.data.uninstalledSdks
    }
};
module.exports = root;