
const cors = require('cors')
const express = require('express')
const typeDefinition = require('./type-definitions')
const root = require('./resolver')
const { graphqlHTTP } = require('express-graphql');
const { buildSchema } = require('graphql');


const schema = buildSchema(typeDefinition);

const port = process.env.PORT || 9000
const app = express()
app.use(cors())
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}))



app.listen(port, () => console.log(`GraphQL is running at ${port}`))