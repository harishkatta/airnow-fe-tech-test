const typeDefinition = `
type Query {
  installed: [installUninstallObj],
  uninstalled:[installUninstallObj]
}
type installUninstallObj{
    id:String,
    name:String,
    categories:[String]!
    firstSeenDate:String,
    lastSeenDate:String
}


`

module.exports = typeDefinition;