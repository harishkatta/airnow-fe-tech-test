
import React, { useState } from "react";
import { NavButtons } from './navbuttons';
import CommonSDK from './commonsdk';
import { useQuery, gql } from "@apollo/client";
import _ from 'lodash'
import { GET_INSTALLED_UNINSTALLED_SDKS } from '../utils/gqlqueries'
export const SDKContext = React.createContext({});

export const Home = (props) => {
  const [showInstalled, setShowInstalled] = useState(true)

  const { loading, error, data } = useQuery(GET_INSTALLED_UNINSTALLED_SDKS);
  let rawData = showInstalled ? data?.installed : data?.uninstalled
  let groupedData = showInstalled ? _.groupBy(data?.installed, 'categories') : _.groupBy(data?.uninstalled, 'categories');
  let count = showInstalled ? data?.installed.length : data?.uninstalled.length;

  let latestDate = _.max(rawData?.map(obj => new Date(obj.lastSeenDate)))
  const orderedData = Object.keys(groupedData).sort().reduce(
    (obj, key) => {
      obj[key] = groupedData[key];
      return obj;
    },
    {}
  );
  if (loading) return <div>loading</div>;

  if (error) return <p>An error occured!</p>;
  return (

    <SDKContext.Provider value={{ showInstalled, setShowInstalled }}>
      <div style={{ backgroundColor: 'white' }}>{showInstalled}</div>
      <NavButtons />
      <CommonSDK data={orderedData} count={count} latestDate={latestDate} />
    </SDKContext.Provider>
  )

}