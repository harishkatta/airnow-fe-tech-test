import React, { useContext } from "react";
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import { SDKContext } from './home'

export const NavButtons = (props) => {
    const { setShowInstalled } = useContext(SDKContext)
    const installedData = () => {
        setShowInstalled(true);
    }
    const unInstalledData = () => {
        setShowInstalled(false);
    }
    return (


        <ButtonGroup variant="outlined" aria-label="outlined button group" sx={{ marginTop: '50px', paddingLeft: '10px', borderRadius: '50px' }}>
            <Button onClick={installedData}>Installed</Button>
            <Button onClick={unInstalledData}>UnInstalled</Button>

        </ButtonGroup>

    )
}