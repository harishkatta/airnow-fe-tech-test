import { render, screen } from '@testing-library/react';
import App from '../App';

test('renders uninstall button', async () => {
    render(<App />);
    const uninstallButton = await screen.findByText(/UnInstalled/i);
    expect(uninstallButton).toBeInTheDocument();
});

test('renders installed button', async () => {
    render(<App />);
    const buttonArray = await screen.findAllByRole('button', { name: 'Installed' });
    expect(buttonArray.length).toBe(1);
});