import React, { useState } from "react";
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import { Box } from '@mui/system';
import { withStyles } from '@mui/styles';

const styles = {
    root: {

    },
    labels: {
        fontFamily: "'sans-serif'",
        fontSize: 15,
        textDecoration: 'underline',
        color: 'skyblue'

    },
    labeldata: {
        fontFamily: "'sans-serif'",
        fontSize: 15,
        color: 'skyblue'
    },
    moveRight: {
        marginLeft: '10px'
    }
};

const CommonSDK = (props) => {
    const { classes } = props;

    return (
        <Box component="span" sx={{ width: 800, height: 400, border: '2px solid skyblue', marginTop: '40px', marginLeft: '10px' }}>
            <Grid container spacing={2}>
                <Grid item xs={10}>
                    <div className={classes.moveRight}><label className={classes.labels}>Installed SDK's</label></div>
                    <div className={classes.moveRight}><label className={classes.labeldata}>Latest Update: {props.latestDate.toLocaleString('default', { month: 'long' }) + ' '}
                        {props.latestDate.getDay()}
                        ,{props.latestDate.getUTCFullYear()}</label></div>
                </Grid>
                <Grid item xs={2}>
                    <div style={{ marginTop: '15px' }}>{props.count}</div>
                </Grid>
            </Grid>
            <Divider sx={{ border: '.5px solid skyblue' }} />
            <Grid container spacing={2} >
                {props.data && Object.keys(props.data).map(item => {
                    return (
                        <Grid item xs={6} justifyContent="space-between">
                            <div justifyContent="space-between" className={classes.moveRight}><label className={classes.labels}>{item}</label></div>
                            {props.data[item].map(obj => {
                                return (
                                    <div className={classes.moveRight}>
                                        <label className={classes.labeldata}>{obj.name}</label>
                                        <label className={classes.labeldata}>{obj.id + ' '}</label>
                                        <label className={classes.labeldata}>lastseen </label>
                                        <label className={classes.labeldata}>{new Date().getUTCFullYear() - new Date(obj.lastSeenDate).getUTCFullYear() + ' years ago'}</label>
                                    </div>
                                )
                            })}
                        </Grid>
                    )
                })}

            </Grid>

        </Box >
    )
}

export default withStyles(styles)(CommonSDK)