import { useQuery, gql } from "@apollo/client";
export const GET_INSTALLED_UNINSTALLED_SDKS = gql`
query getData {
  installed {
    id
    name
    categories
    firstSeenDate
    lastSeenDate
    
  }
  uninstalled {
    id
    name
    categories
    firstSeenDate
    lastSeenDate
    
  }
}`;