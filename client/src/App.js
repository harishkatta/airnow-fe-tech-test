import logo from './logo.svg';

import './App.css';
import { withStyles } from '@mui/styles';


import { useEffect } from 'react';
import gqlClient from './utils/graphql-client';
import { ApolloProvider } from "@apollo/client";
import { Home } from './components/home'

function App(props) {
  // const [data, setData] = useState([])

  // useEffect(() => {

  // }, [])


  return (
    <div className="App">
      <div className="App-body">
        <ApolloProvider client={gqlClient}>
          <Home />
        </ApolloProvider>
      </div>
    </div>
  );
}

// export default withStyles(styles)(App);
export default App
